#include "recorder.h"
#include "ui_recorder.h"

Recorder::Recorder(Configuration *config, Overlay *overlay, QWidget *parent) :
		config(config), overlay(overlay), QWidget(parent), ui(new Ui::Recorder) {
    ui->setupUi(this);
    initVideo();
}

Recorder::~Recorder() {
    delete ui;
    if(sceneVideo)
        delete sceneVideo;
    delete widgetVideo;
    //delete surfaceVideo;
}

//*******  I N I T I A L I Z A T I O N  ************************************************************

void Recorder::initVideo() {
    sceneVideo = new QGraphicsScene(this);
    ui->video->setScene(sceneVideo);
    widgetVideo = new QGst::Ui::VideoWidget;
  //  surfaceVideo = new QGst::Ui::GraphicsVideoSurface(ui->viewRecorder);
  //  graphicWidgetVideo = new QGst::Ui::GraphicsVideoWidget;
  //  graphicWidgetVideo->setSurface(surfaceVideo);
  //  graphicWidgetVideo->setAutoFillBackground(true);
  //  sceneVideo->addItem(graphicWidgetVideo);
  //  graphicWidgetVideo->show();
    sceneVideo->addWidget(widgetVideo);
	config->setVideoWidget(widgetVideo);
    videoFile = config->getVideoFile();
    updateView(0);
   // updateRecorderElements(0);
    populate_Source();
}

void Recorder::populate_Source() {
    QGst::ElementFactoryPtr v4l2src = QGst::ElementFactory::find("v4l2src");
    QGst::ElementFactoryPtr ximagesrc = QGst::ElementFactory::find("ximagesrc");
    ui->SOURCE->addItem("Screen");
	if (!ximagesrc)
        qDebug() << "we don't have ximagesrc, disable the choice to use it";
    else
		ui->SOURCE->addItem("Camera");
}

//*******  A C T I O N S  **************************************************************************

QGst::BinPtr Recorder::createAudioSrcBin() {
    QGst::BinPtr audioSrcBin;
    try {
        audioSrcBin = QGst::Bin::fromDescription("autoaudiosrc name=\"audiosrc\" ! audioconvert ! "
                                              "audioresample ! audiorate ! speexenc ! queue");
    } catch (const QGlib::Error & error) {
        qDebug() << "Failed to create audio source bin:" << error;
        return QGst::BinPtr(); }
  //  QGst::ElementPtr src = audioBin->getElementByName("audiosrc");
    return audioSrcBin;
}

QGst::BinPtr Recorder::createVideoSrcBin(const int &index) {
    QGst::BinPtr videoBin;
    try {
        switch (index) {
          case 0: //screencast
            videoBin = QGst::Bin::fromDescription("ximagesrc name=\"videosrc\" ! "
                                                  "videoconvert ! theoraenc ! queue");
            videoBin->setProperty("screen-num", 0);
            break;
          case 1:  //camera
			QString sourceDescription = QString("%1 name=\"videosrc\" ! videoconvert !"
												" theoraenc ! queue")
										.arg(config->getCodec(Direction::IN, Element::camera));
			videoBin = QGst::Bin::fromDescription(sourceDescription);
			videoBin->setProperty("device", "/dev/video0");
            videoBin->setProperty("camera",0);
			break; }
    } catch (const QGlib::Error & error) {
        qDebug() << "Failed to create video source bin:" << error;
        return QGst::BinPtr(); }
    return videoBin;
}

QGst::ElementPtr Recorder::createVideoSrcElement(const int &index) {
    QGst::ElementPtr videoSrc;
    try {
        switch (index) {
          case 0: //screencast
            videoSrc = QGst::ElementFactory::make("ximagesrc");
            videoSrc->setProperty("name", "videosrc");
			videoSrc->setProperty("screen-num", 0);
            break;
		  case 1:  //camera
			videoSrc = QGst::ElementFactory::make(config->getCodec(Direction::IN, Element::camera));
			videoSrc->setProperty("name", "videosrc");
			break; }
    } catch (const QGlib::Error & error) {
        qDebug() << "Failed to create video source element:" << error;
        return QGst::ElementPtr(); }
    videoSrc->setState(QGst::StateReady);
    return videoSrc;
}

void Recorder::updateView(const int &index) {
    pipelineVideo = QGst::Pipeline::create("recorder-viewer-pipeline");
    if (!pipelineVideo)
        QMessageBox::warning(this, "Failed", "no pipeline created");
    QGst::ElementPtr videoSrc = createVideoSrcElement(index);
//    QGst::ElementPtr sink    = surfaceVideo->videoSink();
    QGst::ElementPtr sink    = QGst::ElementFactory::make("qt5videosink");
    if (!videoSrc || !sink) {
        QMessageBox::warning(this, "Failed", "some elements has not been created");
        exit(-1); }
    pipelineVideo->add(videoSrc, sink);
    if(!videoSrc->link(sink))
        QMessageBox::warning(this, "Failed", "Can not link source with sink");
    pipelineVideo->bus()->addSignalWatch(); // connect the bus
    QGlib::connect(pipelineVideo->bus(), "message",
                   this, &Recorder::onBusVideoMessage);
    widgetVideo->setVideoSink(sink);
    pipelineVideo->setState(QGst::StatePlaying);
}

void Recorder::updateRecorder(const int &index) {
    QGst::BinPtr audioSrcBin = createAudioSrcBin();
    QGst::BinPtr videoSrcBin = createVideoSrcBin(index);
    QGst::ElementPtr mux = QGst::ElementFactory::make("oggmux");
    QGst::ElementPtr sink = QGst::ElementFactory::make("filesink");
    if (!audioSrcBin || !videoSrcBin || !mux || !sink) {
        QMessageBox::critical(this, tr("Error"), tr("One or more elements could not be created. "
                              "Verify that you have all the necessary element plugins installed."));
        return; }
    sink->setProperty("location", videoFile->FullName);
    pipelineRecorder =  QGst::Pipeline::create("recorder-pipeline");
    pipelineRecorder->add(audioSrcBin, videoSrcBin, mux, sink);
    //link elements
    QGst::PadPtr audioSinkPad = mux->getRequestPad("audio_%u");
    QGst::PadPtr audioSrcPad = audioSrcBin->getStaticPad("src");
    audioSrcPad->link(audioSinkPad);
    QGst::PadPtr videoSinkPad = mux->getRequestPad("video_%u");
    QGst::PadPtr videoSrcPad = videoSrcBin->getStaticPad("src");
    videoSrcPad->link(videoSinkPad);
    mux->link(sink);
    pipelineRecorder->bus()->addSignalWatch(); // connect the bus
    QGlib::connect(pipelineRecorder->bus(), "message",
                   this, &Recorder::onBusRecordMessage);
}

//*******  S L O T S  ******************************************************************************

void Recorder::on_SOURCE_currentIndexChanged(int index) {
    pipelineVideo->setState(QGst::StateNull);
	Element src = (ui->SOURCE->currentIndex() == 0) ? Element::screen : Element::camera;
	config->setInputSource(src);
	overlay->initLimits();
    updateView(index);
    updateRecorder(index);
}

void Recorder::on_PAUSE_toggled(bool checked) {
    (checked)
        ? pipelineRecorder->setState(QGst::StatePaused)
        : pipelineRecorder->setState(QGst::StatePlaying);
}

void Recorder::on_STOP_clicked() {
    pipelineRecorder->setState(QGst::StateReady);
}

void Recorder::on_RECORD_toggled(bool checked) {
    /* send an end-of-stream event to flush metadata and cause an EosMessage deliver */
    if (pipelineRecorder)
        pipelineRecorder->sendEvent(QGst::EosEvent::create());
    else { // else if pipeline doesn't exist - start a new one
        pipelineRecorder->setState(QGst::StatePlaying); // go!
        ui->RECORD->setText(tr("Stop recording")); }
}


//*******  B U S  **********************************************************************************

void Recorder::onBusVideoMessage(const QGst::MessagePtr & message) {
    switch (message->type()) {
      case QGst::MessageEos:
        QMessageBox::warning(this, tr("Stop recording"),
                             message.staticCast<QGst::EosMessage>()->typeName());
        break;
      case QGst::MessageError:
        QMessageBox::critical(this, tr("Pipeline Recorder VIEW Error"),
                              message.staticCast<QGst::ErrorMessage>()->error().message());
        break;
      default:
        break; }
}

void Recorder::onBusRecordMessage(const QGst::MessagePtr & message) {
    switch (message->type()) {
    case QGst::MessageEos:
        stop();  //got end-of-stream - stop the pipeline
        QMessageBox::warning(this, tr("Stop recording"),
                             message.staticCast<QGst::EosMessage>()->typeName());
        break;
    case QGst::MessageError:
        if (pipelineRecorder)  //check if the pipeline exists
            stop();
        QMessageBox::critical(this, tr("Pipeline Recorder Error"),
                              message.staticCast<QGst::ErrorMessage>()->error().message());
        break;
    default:
        break; }
}

void Recorder::stop() {
    pipelineRecorder->setState(QGst::StateNull); //stop recording
    ui->RECORD->setText(tr("Start recording"));
    ui->RECORD->setChecked(false);
}
