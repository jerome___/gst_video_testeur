#ifndef DATAS_H
#define DATAS_H

#include <QString>
#include <QFile>

struct Datas {
    QString     FileName;
    QString     Directory;
    QString     Extention;
    QString     FullName;
    QFile       File;
};

enum Direction { IN, OUT };
enum Element { screen, camera };

#endif // DATAS_H
