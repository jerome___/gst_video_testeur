#include "overlay.h"
#include "ui_overlay.h"

Overlay::Overlay(Configuration *config, QWidget *parent) :
	config(config), QWidget(parent), ui(new Ui::Overlay) {
	ui->setupUi(this);
	initLimits();
}

Overlay::~Overlay() {
    delete ui;
	delete config;
	delete overlayFile;
}

//*******  I N I T I A L I Z A T I O N  ************************************************************

void Overlay::initLimits() {
	ui->posX_slider->setMaximum(config->getSrcWidth());
	ui->posY_slider->setMaximum(config->getSrcHeight());
	ui->PosX->setMaximum(config->getSrcWidth());
	ui->PosY->setMaximum(config->getSrcHeight());
}

//*******  A C T I O N S  **************************************************************************

//*******  S L O T S  ******************************************************************************

void Overlay::on_Entries_clicked(const QModelIndex &index) {

}

void Overlay::on_ADD_clicked() {

}

void Overlay::on_DEL_clicked() {

}

void Overlay::on_ChooseColor_clicked() {

}

void Overlay::on_ChooseFont_clicked() {

}

void Overlay::on_COPY_clicked() {

}

void Overlay::on_EDIT_clicked() {

}

void Overlay::on_REC_clicked() {

}
