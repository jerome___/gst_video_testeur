#ifndef RECORDER_H
#define RECORDER_H

#include <QWidget>
#include <QMessageBox>
#include <QDir>
#include <QFileDialog>
#include <QDebug>

#include <QGlib/Connect>
#include <QGlib/Error>
#include <QGst/Pipeline>
#include <QGst/ElementFactory>
#include <QGst/Pad>
#include <QGst/Bus>
#include <QGst/Message>
#include <QGst/Query>
#include <QGst/ClockTime>
#include <QGst/Event>
#include <QGst/StreamVolume>
#include <QGst/Ui/VideoWidget>
#include <QGst/Ui/GraphicsVideoSurface>
#include <QGst/Ui/GraphicsVideoWidget>
#include <QGst/videooverlay.h>

#include "datas.h"
#include "configuration.h"
#include "overlay.h"

namespace Ui { class Recorder; }

class Recorder : public QWidget {
    Q_OBJECT

public:
	explicit Recorder(Configuration* config,
					  Overlay* overlay,
					  QWidget *parent = 0);
    ~Recorder();

private slots:
    void on_SOURCE_currentIndexChanged(int index);
    void on_PAUSE_toggled(bool checked);
    void on_STOP_clicked();
    void on_RECORD_toggled(bool checked);

private:
    Ui::Recorder            *ui;
    QGraphicsScene          *sceneVideo;
    QGst::Ui::VideoWidget   *widgetVideo;
    //   QGst::Ui::GraphicsVideoWidget   *graphicWidgetVideo;
    //   QGst::Ui::GraphicsVideoSurface  *surfaceVideo;
    QGst::PipelinePtr        pipelineVideo, pipelineRecorder;
    Configuration           *config;
	Overlay					*overlay;
    Datas                   *videoFile, *overlayFile;
    void             initVideo();
    void             populate_Source();
    void             updateView(const int &index = 0);
    void             updateRecorder(const int &index);
    QGst::BinPtr     createAudioSrcBin();
    QGst::BinPtr     createVideoSrcBin(const int &index = 0);
    QGst::ElementPtr createVideoSrcElement(const int &index = 0);
    void onBusVideoMessage(const QGst::MessagePtr & message);
    void onBusRecordMessage(const QGst::MessagePtr & message);
    void stop();
};

#endif // RECORDER_H
