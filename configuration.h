#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <QWidget>
#include <QFileDialog>
#include <QComboBox>
#include <QSpinBox>
#include <QLineEdit>
#include <QX11Info>
#include <QDesktopWidget>

#include "datas.h"

namespace Ui { class Configuration; }

class Configuration : public QWidget {
    Q_OBJECT

public:
    explicit Configuration(QWidget *parent = 0);
    ~Configuration();
	Datas*        getOverlayVideoFile() const;
	Datas*        getVideoFile() const;
	QString       getCodec(Direction dir, Element elem) const;
	void          setInputSource(const Element &elem);
	const Element getInputSource() const;
	void          setVideoWidget(QWidget *video);
	QWidget*      getVideoWidget();
	int			  getSrcWidth();
	int			  getSrcHeight();

signals:
    void directoryEntered(QString);

private slots:
	void on_DirectoryVideoChoose_clicked();
    void on_FileNameVideo_textChanged(const QString &arg1);
	void on_FileNameOverlayVideo_textChanged(const QString &arg1);
    void on_ExtentionVideo_currentIndexChanged(const QString &arg1);
	void on_DirectoryPhotoChoose_clicked();
	void on_FileNamePhoto_textChanged(const QString &arg1);
	void on_FileNameOverlayPhoto_textChanged(const QString &arg1);
	void on_ExtentionPhoto_currentIndexChanged(const QString &arg1);
    void limitToCampaignDir(QString dir);

private:
    Ui::Configuration *ui;
	Datas             *videoFile, *overlayVideo, *photoFile, *overlayPhoto;
    QFileDialog       *dialogFile;
	Element			   inputSource;
	int				   inputSrcWidth, inputSrcHeight;
	QWidget			  *videoWidget;
	QDesktopWidget    *wind ;
    void         populateExtentionVideoList();
	void         populateCameraCodecs();
};

#endif // CONFIGURATION_H
