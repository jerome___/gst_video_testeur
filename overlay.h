#ifndef OVERLAY_H
#define OVERLAY_H

#include <QWidget>
#include <QSlider>
#include <QSpinBox>
#include <QMessageBox>
#include <QDir>
#include <QFileDialog>
#include <QDebug>
#include <QX11Info>
#include <QDesktopWidget>

#include <QGlib/Connect>
#include <QGlib/Error>
#include <QGst/Pipeline>
#include <QGst/ElementFactory>
#include <QGst/Pad>
#include <QGst/Bus>
#include <QGst/Message>
#include <QGst/Query>
#include <QGst/ClockTime>
#include <QGst/Event>
#include <QGst/StreamVolume>
#include <QGst/Ui/VideoWidget>
#include <QGst/Ui/GraphicsVideoSurface>
#include <QGst/Ui/GraphicsVideoWidget>
#include <QGst/videooverlay.h>

#include "datas.h"
#include "configuration.h"

namespace Ui { class Overlay; }

class Overlay : public QWidget {
    Q_OBJECT

public:
    explicit Overlay(Configuration *config, QWidget *parent = 0);
    ~Overlay();
	void initLimits();

private slots:
    void on_Entries_clicked(const QModelIndex &index);
    void on_ADD_clicked();
    void on_DEL_clicked();
    void on_ChooseColor_clicked();
    void on_ChooseFont_clicked();
    void on_EDIT_clicked();
    void on_REC_clicked();
    void on_COPY_clicked();

private:
    Ui::Overlay *ui;
    Configuration           *config;
	Datas                   *overlayFile;
};

#endif // OVERLAY_H
