#-------------------------------------------------
#
# Project created by QtCreator 2016-08-17T07:21:24
#
#-------------------------------------------------

QT       += core gui x11extras widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GST_Video_Testeur
TEMPLATE = app
CONFIG += link_pkgconfig
PKGCONFIG +=  Qt5GLib-2.0 \
    Qt5GStreamer-1.0 \
    Qt5GStreamerUi-1.0 \
    Qt5GStreamerUtils-1.0


SOURCES += main.cpp\
        mainwindow.cpp \
    reader.cpp \
    recorder.cpp \
    overlay.cpp \
    configuration.cpp

HEADERS  += mainwindow.h \
    reader.h \
    recorder.h \
    overlay.h \
    configuration.h \
    datas.h

FORMS    += mainwindow.ui \
    reader.ui \
    recorder.ui \
    overlay.ui \
    configuration.ui
