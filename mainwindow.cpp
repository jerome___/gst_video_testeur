#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
    ui->setupUi(this);
    initTabs();
}

MainWindow::~MainWindow() {
    delete ui;
    delete reader;
    delete recorder;
    delete overlay;
    delete configuration;
}

//*****  I N I T I A L I S A T I O N  **************************************************************

void MainWindow::initTabs() {
    configuration = new Configuration(this);
	overlay       = new Overlay(configuration, this);
	reader        = new Reader(configuration, this);
	recorder      = new Recorder(configuration, overlay, this);
    ui->reader->layout()->addWidget(reader);
    ui->recorder->layout()->addWidget(recorder);
    ui->overlay->layout()->addWidget(overlay);
    ui->configuration->layout()->addWidget(configuration);
}

//*****  A C T I O N S  ****************************************************************************
