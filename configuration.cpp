#include "configuration.h"
#include "ui_configuration.h"

Configuration::Configuration(QWidget *parent) :
        QWidget(parent), ui(new Ui::Configuration) {
    ui->setupUi(this);
    videoFile = new Datas;
	overlayVideo = new Datas;
	photoFile = new Datas;
	overlayPhoto = new Datas;
	ui->DirectoryVideo->setText(QDir::homePath());
	ui->FileNameVideo->setText("testVideo");
	ui->FileNameOverlayVideo->setText("testVideoOverlay");
	ui->DirectoryPhoto->setText(QDir::homePath());
	ui->FileNamePhoto->setText("testPhoto");
	ui->FileNameOverlayPhoto->setText("testPhotoOverlay");
	wind = new QDesktopWidget();
	inputSrcWidth = wind->availableGeometry().width();
	inputSrcHeight = wind->availableGeometry().height();
	ui->OVPHoto->hide();
	ui->FileNameOverlayPhoto->hide();
    populateExtentionVideoList();
	populateCameraCodecs();
}

Configuration::~Configuration() {
    delete ui;
	delete videoFile;
	delete overlayVideo;
	delete photoFile;
	delete overlayPhoto;
	delete wind;
	delete videoWidget;
}

//*****  A C T I O N S  ****************************************************************************

void Configuration::limitToCampaignDir(QString dir) {
    QRegExp rgx(QDir::homePath());
    if (rgx.indexIn(dir) == -1)
        dialogFile->setDirectory(QDir::homePath());
}

void Configuration::populateExtentionVideoList() {
	ui->ExtentionVideo->addItem("ogv");
}

void Configuration::populateCameraCodecs() {
	ui->cameras_codec->addItem("autovideosrc");
	ui->cameras_codec->addItem("v4l2src");
}

//*******  P U B L I C    A C T I O N S  ***********************************************************

Datas* Configuration::getOverlayVideoFile() const {
	return overlayVideo;
}

Datas* Configuration::getVideoFile() const {
    return videoFile;
}

QString Configuration::getCodec(Direction dir, Element elem) const {
	switch (dir) {
		case Direction::IN:
		  if (elem == Element::camera)
			return ui->cameras_codec->currentText();
		  else
			return QString("%1").arg(ui->screen_in_number->value());
		  break;
		case Direction::OUT:
		  break; }
}

void Configuration::setInputSource(const Element &elem) {
	inputSource = elem;
	const QRect sourceSize = (inputSource == Element::screen)
						   ? wind->availableGeometry()
						   : wind->availableGeometry(getVideoWidget());
	inputSrcWidth = sourceSize.width();
	inputSrcHeight = sourceSize.height();
}

const Element Configuration::getInputSource() const {
  return inputSource;
}

void Configuration::setVideoWidget(QWidget *video) {
	videoWidget = video;
}

QWidget* Configuration::getVideoWidget() {
	return videoWidget;
}

int	Configuration::getSrcWidth() {
	return inputSrcWidth;
}

int	Configuration::getSrcHeight() {
	return inputSrcHeight;
}

//*******  S L O T S  ******************************************************************************

void Configuration::on_DirectoryVideoChoose_clicked() {
    dialogFile = new QFileDialog(this,
					 tr("Choisir un répertoire d'enregistrement des vidéos"),
                     QDir::homePath());
    connect(dialogFile, SIGNAL(directoryEntered(QString)),
            this, SLOT(limitToCampaignDir(QString)));
    dialogFile->setOption(QFileDialog::ShowDirsOnly);
    dialogFile->setFileMode(QFileDialog::Directory);
    int answer = dialogFile->exec();
    if (answer == 1)
		ui->DirectoryVideo->setText(dialogFile->directory().absolutePath());
}

void Configuration::on_FileNameVideo_textChanged(const QString &arg1) {
    videoFile->FileName = arg1;
	videoFile->Directory = ui->DirectoryVideo->text();
    videoFile->Extention = ui->ExtentionVideo->currentText();
    videoFile->FullName = videoFile->Directory +
                           "/" + arg1 + "." +
                           videoFile->Extention;
}

void Configuration::on_FileNameOverlayVideo_textChanged(const QString &arg1) {
	overlayVideo->FileName = arg1;
	overlayVideo->Directory = ui->DirectoryVideo->text();
	overlayVideo->Extention = ".xml";
	overlayVideo->FullName = overlayVideo->Directory + "/" + arg1 + ".xml";
}

void Configuration::on_ExtentionVideo_currentIndexChanged(const QString &arg1) {
	videoFile->Extention = arg1;
	videoFile->Directory = ui->DirectoryVideo->text();
	videoFile->FileName = ui->FileNameVideo->text();
	videoFile->FullName = videoFile->Directory +
						  "/" + videoFile->FileName +
						  "." + arg1;
}

void Configuration::on_DirectoryPhotoChoose_clicked() {
	dialogFile = new QFileDialog(this,
					 tr("Choisir un répertoire d'enregistrement des photos"),
					 QDir::homePath());
	connect(dialogFile, SIGNAL(directoryEntered(QString)),
			this, SLOT(limitToCampaignDir(QString)));
	dialogFile->setOption(QFileDialog::ShowDirsOnly);
	dialogFile->setFileMode(QFileDialog::Directory);
	int answer = dialogFile->exec();
	if (answer == 1)
		ui->DirectoryPhoto->setText(dialogFile->directory().absolutePath());
}

void Configuration::on_FileNamePhoto_textChanged(const QString &arg1) {
	photoFile->FileName = arg1;
	photoFile->Directory = ui->DirectoryPhoto->text();
	photoFile->Extention = ui->ExtentionPhoto->currentText();
	photoFile->FullName = photoFile->Directory +
						   "/" + arg1 + "." +
						   photoFile->Extention;
}

void Configuration::on_FileNameOverlayPhoto_textChanged(const QString &arg1) {
	overlayPhoto->FileName = arg1;
	overlayPhoto->Directory = ui->DirectoryPhoto->text();
	overlayPhoto->Extention = ".xml";
	overlayPhoto->FullName = overlayPhoto->Directory + "/" + arg1 + ".xml";
}

void Configuration::on_ExtentionPhoto_currentIndexChanged(const QString &arg1) {
	photoFile->Extention = arg1;
	photoFile->Directory = ui->DirectoryPhoto->text();
	photoFile->FileName = ui->FileNamePhoto->text();
	photoFile->FullName = photoFile->Directory +
						  "/" + photoFile->FileName +
						  "." + arg1;
}
