#include "reader.h"
#include "ui_reader.h"

Reader::Reader(Configuration *config, QWidget *parent) :
        config(config), QWidget(parent), ui(new Ui::Reader) {
    ui->setupUi(this);
}

Reader::~Reader() {
    delete ui;
}

//*******  I N I T I A L I Z A T I O N  ************************************************************

//*******  A C T I O N S  **************************************************************************

//*******  S L O T S  ******************************************************************************

void Reader::on_RWD_clicked() {

}

void Reader::on_PLAY_clicked() {

}

void Reader::on_PAUSE_toggled(bool checked) {

}

void Reader::on_STOP_clicked() {

}

void Reader::on_FFWD_clicked() {

}
