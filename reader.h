#ifndef READER_H
#define READER_H

#include <QWidget>

#include <QGlib/Connect>
#include <QGlib/Error>
#include <QGst/Pipeline>
#include <QGst/ElementFactory>
#include <QGst/Pad>
#include <QGst/Bus>
#include <QGst/Message>
#include <QGst/Query>
#include <QGst/ClockTime>
#include <QGst/Event>
#include <QGst/StreamVolume>
#include <QGst/Ui/VideoWidget>
#include <QGst/Ui/GraphicsVideoSurface>
#include <QGst/Ui/GraphicsVideoWidget>
#include <QGst/videooverlay.h>

#include "datas.h"
#include "configuration.h"

namespace Ui { class Reader; }

class Reader : public QWidget {
    Q_OBJECT

public:
    explicit Reader(Configuration* config, QWidget *parent = 0);
    ~Reader();

private slots:
    void on_RWD_clicked();
    void on_PLAY_clicked();
    void on_PAUSE_toggled(bool checked);
    void on_STOP_clicked();
    void on_FFWD_clicked();

private:
    Ui::Reader              *ui;
    Configuration           *config;
    QGraphicsScene          *sceneVideo;
    QGst::Ui::VideoWidget   *widgetVideo;
    //   QGst::Ui::GraphicsVideoWidget   *graphicWidgetVideo;
    //   QGst::Ui::GraphicsVideoSurface  *surfaceVideo;
    QGst::PipelinePtr        pipeline;
    Datas                   *videoFile, *overlayFile;
};

#endif // READER_H
